from django.urls import path, include

from pos.views import POSView
urlpatterns = [
    path('', POSView.as_view(), name='pos'),
]