from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.
from django.shortcuts import redirect
from django.views.generic import TemplateView


class POSView(LoginRequiredMixin, TemplateView):
    template_name = "pos/pos_front.html"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return redirect('/account/admin')

        return super(POSView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(POSView, self).get_context_data()
        context['title'] = "POS"
        context['pos'] = "active"

        return context