from rest_framework.relations import StringRelatedField
from rest_framework.serializers import ModelSerializer

from products.models import Brand, Category, Product


class BrandRetrieveByCategorySerializer(ModelSerializer):
    category = StringRelatedField()
    class Meta:
        model = Brand
        fields = ('id', 'name', 'category')


class CategoryListSerializer(ModelSerializer):
    category_brands = BrandRetrieveByCategorySerializer(many=True, read_only=True)
    class Meta:
        model = Category
        fields = ('id', 'name', 'category_brands')


class ProductRetrieveByBrandSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name', 'picture', 'unit_price', 'status')


class ProductRetrieveSerializer(ModelSerializer):

    class Meta:
        model = Product
        fields = ('id', 'name', 'unit_price')



