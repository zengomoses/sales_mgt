from django.urls import path
from pos.api.views import BrandRetrieveByCategoryAPIView, CategoryListAPIView, ProductRetrieveByBrandAPIView, \
    ProductRetrieveAPIView

urlpatterns = [
    path('product/categories/', CategoryListAPIView.as_view()),
    path('product/brands/<int:category_id>', BrandRetrieveByCategoryAPIView.as_view()),
    path('product/<int:brand_id>', ProductRetrieveByBrandAPIView.as_view()),
    path('product/retrieve/<int:pk>', ProductRetrieveAPIView.as_view()),
]