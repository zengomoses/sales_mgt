from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework.generics import RetrieveAPIView, ListAPIView

from pos.api.serializers import BrandRetrieveByCategorySerializer, CategoryListSerializer, \
    ProductRetrieveByBrandSerializer, ProductRetrieveSerializer
from products.models import Brand, Category, Product


class CategoryListAPIView(LoginRequiredMixin, ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategoryListSerializer


class BrandRetrieveByCategoryAPIView(LoginRequiredMixin, ListAPIView):
    queryset = Brand.objects.all()
    serializer_class = BrandRetrieveByCategorySerializer
    lookup_field = 'category_id'


class ProductRetrieveByBrandAPIView(LoginRequiredMixin, ListAPIView):
    serializer_class = ProductRetrieveByBrandSerializer

    def get_queryset(self):
        """
        This view should return a list of all the products for each brand.
        """

        brand_id = self.kwargs['brand_id']
        queryset = Product.objects.filter(brand=brand_id)

        return queryset


class ProductRetrieveAPIView(LoginRequiredMixin, RetrieveAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductRetrieveSerializer
