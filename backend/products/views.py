from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render


# Create your views here.
from django.views.generic import TemplateView


class ProductListView(LoginRequiredMixin, TemplateView):
    template_name = "products/product_list.html"

    def get_context_data(self, **kwargs):
        context = super(ProductListView, self).get_context_data()
        context['title'] = "Products"
        context['product_list'] = "active"

        return context


class CategoryListView(LoginRequiredMixin, TemplateView):
    template_name = "products/category_list.html"

    def get_context_data(self, **kwargs):
        context = super(CategoryListView, self).get_context_data()
        context['title'] = "Categories"
        context['category_list'] = "active"

        return context


class BrandListView(LoginRequiredMixin, TemplateView):
    template_name = "products/brand_list.html"

    def get_context_data(self, **kwargs):
        context = super(BrandListView, self).get_context_data()
        context['title'] = "Brands"
        context['brand_list'] = "active"

        return context