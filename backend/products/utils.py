import random
from datetime import datetime

def generate_stock_number():
    letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
               'W', 'Y', 'Z']
    string_code = ''.join(random.choice(letters) for m in range(4))
    date = datetime.today().strftime('%Y%m%d')
    code = string_code + date
    return code
