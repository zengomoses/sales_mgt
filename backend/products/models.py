from django.db import models
from django.utils import timezone


class Category(models.Model):
    STATUS_CHOICE = (
        ('Active', 'Active'),
        ('Inactive', 'Inactive')
    )
    name = models.CharField(max_length=100, db_index=True, unique=True)
    status = models.CharField(max_length=100, choices=STATUS_CHOICE, default='Active')
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return '{}'.format(self.name)


class Brand(models.Model):
    STATUS_CHOICE = (
        ('Active', 'Active'),
        ('Inactive', 'Inactive')
    )
    name = models.CharField(max_length=100, db_index=True, unique=True)
    category = models.ForeignKey(Category, related_name='category_brands', on_delete=models.CASCADE)
    status = models.CharField(max_length=100, choices=STATUS_CHOICE,  default='Active')
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = 'Brands'

    def __str__(self):
        return '{}'.format(self.name)


class Product(models.Model):
    STATUS_CHOICE = (
        ('In Stock', 'In Stock'),
        ('Out of Stock', 'Out of Stock')
    )
    name = models.CharField(max_length=100)
    serial_number = models.CharField(max_length=20, db_index=True, unique=True, blank=True, null=True)
    picture = models.TextField(null=True, blank=True)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    unit_price = models.FloatField(default=0)
    incoming_inventory = models.IntegerField(default=0)
    outgoing_inventory = models.IntegerField(default=0)
    minimum_required = models.IntegerField(default=0)
    status = models.CharField(max_length=100, choices=STATUS_CHOICE, default='Out of Stock')
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = 'Products'
        unique_together = ('name', 'serial_number')

    def __str__(self):
        return '{} {}'.format(self.brand, self.name)
