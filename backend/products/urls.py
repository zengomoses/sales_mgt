from django.urls import path

from products.views import ProductListView, CategoryListView, BrandListView

urlpatterns = [
    path('list', ProductListView.as_view(), name='product_list'),
    path('category', CategoryListView.as_view(), name='category_list'),
    path('brand', BrandListView.as_view(), name='brand_list'),
]