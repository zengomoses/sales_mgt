from django.urls import path

from products.api.views import CategoryCreateView, CategoryListView, CategoryRetrieveView, CategoryUpdateView, \
    BrandCreateView, BrandListView, BrandRetrieveView, BrandUpdateView, ProductCreateView, ProductListView, \
    ProductRetrieveView, ProductUpdateView

app_name = 'Products'

urlpatterns = [
    path('category/create/', CategoryCreateView.as_view()),
    path('category/list/', CategoryListView.as_view()),
    path('category/retrieve/<int:pk>', CategoryRetrieveView.as_view()),
    path('category/update/<int:pk>', CategoryUpdateView.as_view()),

    path('brand/create/', BrandCreateView.as_view()),
    path('brand/list/', BrandListView.as_view()),
    path('brand/retrieve/<int:pk>', BrandRetrieveView.as_view()),
    path('brand/update/<int:pk>', BrandUpdateView.as_view()),

    path('product/create/', ProductCreateView.as_view()),
    path('product/list/', ProductListView.as_view()),
    path('product/retrieve/<int:pk>', ProductRetrieveView.as_view()),
    path('product/update/<int:pk>', ProductUpdateView.as_view()),
]