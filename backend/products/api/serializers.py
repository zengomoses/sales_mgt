from rest_framework.relations import StringRelatedField
from rest_framework.serializers import ModelSerializer

from products.models import Category, Brand, Product


class CategoryCreateSerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class CategoryListSerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class CategoryRetrieveSerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class CategoryUpdateSerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class BrandCreateSerializer(ModelSerializer):
    class Meta:
        model = Brand
        fields = '__all__'


class BrandListSerializer(ModelSerializer):
    category = StringRelatedField()

    class Meta:
        model = Brand
        fields = '__all__'


class BrandRetrieveSerializer(ModelSerializer):
    class Meta:
        model = Brand
        fields = '__all__'


class BrandUpdateSerializer(ModelSerializer):
    class Meta:
        model = Brand
        fields = '__all__'


class ProductCreateSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class ProductListSerializer(ModelSerializer):
    brand = BrandListSerializer()

    class Meta:
        model = Product
        fields = '__all__'


class ProductRetrieveSerializer(ModelSerializer):
    brand = StringRelatedField()

    class Meta:
        model = Product
        fields = '__all__'


class ProductUpdateSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'
