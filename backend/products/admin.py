from django.contrib import admin

# Register your models here.
from products.models import Product, Category, Brand


class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'serial_number', 'brand', 'unit_price', 'incoming_inventory',
                    'outgoing_inventory', 'minimum_required', 'date_created', 'status')


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'status')


class BrandAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'category', 'status')


admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Brand, BrandAdmin)
