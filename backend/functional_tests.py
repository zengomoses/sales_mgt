from selenium import webdriver
import unittest


class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_can_start_app(self):
        # User wants to get access to the app. He/she decides to launch the app
        self.browser.get('http://127.0.0.1:8000')

        # He/she notices the page title and header mention SIMS-KASANDA
        self.assertIn('SIMS-KASANDA', self.browser.title)
        self.fail('Finish the test!')


if __name__=='__main__':
    unittest.main(warnings='ignore')


