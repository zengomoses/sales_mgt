from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView


class EmployeeListView(LoginRequiredMixin, TemplateView):

    template_name = "employee/employee_list.html"

    def get_context_data(self, **kwargs):
        context = super(EmployeeListView, self).get_context_data()
        context['title'] = "Employees"
        context['employees'] = "active"

        return context
