from rest_framework.generics import CreateAPIView, RetrieveAPIView, ListAPIView, UpdateAPIView
from rest_framework.permissions import IsAuthenticated

from employee.api.serializers import EmployeeProfileCreateSerializer, EmployeeProfileRetrieveSerializer, \
    EmployeePositionCreateSerializer, EmployeePositionRetrieveSerializer, EmployeePositionListSerializer, \
    EmployeeSalaryListSerializer, EmployeeListSerializer, EmployeeSalaryRetrieveSerializer, \
    EmployeeSalaryCreateSerializer, EmployeePositionUpdateSerializer, EmployeeSalaryUpdateSerializer, \
    EmployeeProfileUpdateSerializer
from employee.models import EmployeeProfile, EmployeePosition, EmployeeSalary


class EmployeePositionCreateView(CreateAPIView):
    """
    Creates a new employee position
    """

    queryset = EmployeePosition.objects.all()
    serializer_class = EmployeePositionCreateSerializer


class EmployeePositionListView(ListAPIView):
    """
    List all existing employee positions
    """
    queryset = EmployeePosition.objects.all()
    serializer_class = EmployeePositionListSerializer


class EmployeePositionRetrieveView(RetrieveAPIView):
    """
    Retrieve details of a single employee position
    """
    queryset = EmployeePosition
    serializer_class = EmployeePositionRetrieveSerializer
    permission_classes = [IsAuthenticated, ]


class EmployeePositionUpdateView(UpdateAPIView):
    """
    Update details of a single employee position
    """
    queryset = EmployeePosition
    serializer_class = EmployeePositionUpdateSerializer


class EmployeeSalaryCreateView(CreateAPIView):
    """
    Add a employee salary
    """
    queryset = EmployeeSalary.objects.all()
    serializer_class = EmployeeSalaryCreateSerializer


class EmployeeSalaryListView(ListAPIView):
    """
    List all employee salaries
    """
    queryset = EmployeeSalary.objects.all()
    serializer_class = EmployeeSalaryListSerializer


class EmployeeSalaryRetrieveView(RetrieveAPIView):
    """
    Retrieve employee's salaries
    """
    queryset = EmployeeSalary
    serializer_class = EmployeeSalaryRetrieveSerializer


class EmployeeSalaryUpdateView(UpdateAPIView):
    """
    Retrieve employee's salaries
    """
    queryset = EmployeeSalary
    serializer_class = EmployeeSalaryUpdateSerializer


class EmployeeProfileCreateView(CreateAPIView):
    """
    Create employee Profile
    """
    queryset = EmployeeProfile.objects.all()
    serializer_class = EmployeeProfileCreateSerializer


class EmployeeListView(ListAPIView):
    """
    List all employees Profiles
    """
    queryset = EmployeeProfile.objects.all()
    serializer_class = EmployeeListSerializer


class EmployeeProfileRetrieveView(RetrieveAPIView):
    """
    Retrieve employee's user account Profiles
    """
    queryset = EmployeeProfile
    serializer_class = EmployeeProfileRetrieveSerializer

    lookup_field = 'pk'


class EmployeeProfileUpdateView(UpdateAPIView):
    """
    Update employee's  Profiles
    """
    queryset = EmployeeProfile
    serializer_class = EmployeeProfileUpdateSerializer
    lookup_field = 'employee'
