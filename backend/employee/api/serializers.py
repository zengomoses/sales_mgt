from django.contrib.auth.models import User
from rest_framework.relations import StringRelatedField
from rest_framework.serializers import ModelSerializer

from employee.models import EmployeeProfile, EmployeePosition, EmployeeSalary


class EmployeeAccountListSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')


class EmployeeAccountCreateSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')


class EmployeePositionCreateSerializer(ModelSerializer):
    class Meta:
        model = EmployeePosition
        fields = '__all__'


class EmployeePositionListSerializer(ModelSerializer):
    class Meta:
        model = EmployeePosition
        fields = '__all__'


class EmployeePositionRetrieveSerializer(ModelSerializer):
    class Meta:
        model = EmployeePosition
        fields = '__all__'


class EmployeePositionUpdateSerializer(ModelSerializer):
    class Meta:
        model = EmployeePosition
        fields = '__all__'


class EmployeeProfileCreateSerializer(ModelSerializer):
    class Meta:
        model = EmployeeProfile
        fields = '__all__'
        read_only_fields = ('id', 'date_joined', 'is_active', 'is_staff', 'user_permissions', 'groups',
                            'last_login', 'is_superuser')
        extra_kwargs = {'password': {'write_only': True}}


class EmployeeListSerializer(ModelSerializer):
    position = StringRelatedField()
    class Meta:
        model = EmployeeProfile
        fields = '__all__'


class EmployeeProfileRetrieveSerializer(ModelSerializer):
    # employee = EmployeeAccountListSerializer()
    position = EmployeePositionListSerializer()

    class Meta:
        model = EmployeeProfile
        fields = '__all__'


class EmployeeProfileUpdateSerializer(ModelSerializer):
    employee = EmployeeAccountListSerializer()
    position = EmployeePositionListSerializer()

    class Meta:
        model = EmployeeProfile
        fields = '__all__'


class EmployeeSalaryCreateSerializer(ModelSerializer):
    class Meta:
        model = EmployeeSalary
        fields = '__all__'


class EmployeeSalaryListSerializer(ModelSerializer):
    employee = StringRelatedField()

    class Meta:
        model = EmployeeSalary
        fields = '__all__'


class EmployeeSalaryRetrieveSerializer(ModelSerializer):
    employee = StringRelatedField()

    class Meta:
        model = EmployeeSalary
        fields = '__all__'


class EmployeeSalaryUpdateSerializer(ModelSerializer):
    employee = StringRelatedField()

    class Meta:
        model = EmployeeSalary
        fields = '__all__'


