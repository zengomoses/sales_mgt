from django.urls import path
from rest_framework_swagger.views import get_swagger_view

from employee.api.views import EmployeeProfileCreateView, EmployeeProfileRetrieveView, EmployeeListView, \
    EmployeePositionCreateView, EmployeePositionListView, EmployeePositionRetrieveView, EmployeeSalaryCreateView, \
    EmployeeSalaryListView, EmployeeSalaryRetrieveView, EmployeeProfileUpdateView, EmployeePositionUpdateView, \
    EmployeeSalaryUpdateView

app_name = 'Employee'

urlpatterns = [
    path('profile/create/', EmployeeProfileCreateView.as_view()),
    path('profile/list/', EmployeeListView.as_view()),
    path('profile/retrieve/<int:pk>', EmployeeProfileRetrieveView.as_view()),
    path('profile/update/<int:employee>', EmployeeProfileUpdateView.as_view()),

    path('position/create/', EmployeePositionCreateView.as_view()),
    path('position/list/', EmployeePositionListView.as_view()),
    path('position/retrieve/<int:pk>', EmployeePositionRetrieveView.as_view()),
    path('position/update/<int:pk>', EmployeePositionUpdateView.as_view()),

    path('salary/create/', EmployeeSalaryCreateView.as_view()),
    path('salary/list/', EmployeeSalaryListView.as_view()),
    path('salary/retrieve/<int:pk>', EmployeeSalaryRetrieveView.as_view()),
    path('salary/update/<int:pk>', EmployeeSalaryUpdateView.as_view()),
]
