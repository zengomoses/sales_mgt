from django.contrib import admin
from employee.models import EmployeePosition, EmployeeProfile, EmployeeSalary


class EmployeePositionAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'salary_scale', 'date_created')


class EmployeeSalaryAdmin(admin.ModelAdmin):
    list_display = ('id', 'employee', 'date_created', 'status')


class EmployeeProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_name', 'last_name', 'gender', 'marital_status', 'phone_number', 'birth_date', 'position', 'is_active', 'end_date')


admin.site.register(EmployeeSalary, EmployeeSalaryAdmin)
admin.site.register(EmployeeProfile, EmployeeProfileAdmin)
admin.site.register(EmployeePosition, EmployeePositionAdmin)
