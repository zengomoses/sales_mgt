from django.contrib.auth.hashers import is_password_usable, make_password
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


class EmployeePosition(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=255)
    salary_scale = models.FloatField(default=0)
    date_created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'Employee Positions'

    def __str__(self):
        return '{}'.format(self.name)


class EmployeeProfile(User):
    GENDER_CHOICES = (
        ('Male', 'Male'),
        ('Female', 'Female')
    )
    MARITAL_STATUS = (
        ('Single', 'Single'),
        ('Married', 'Married'),
        ('Divorced', 'Divorced'),
        ('Widowed', 'Widowed')
    )

    gender = models.CharField(max_length=20, choices=GENDER_CHOICES)
    marital_status = models.CharField(max_length=20, choices=MARITAL_STATUS)
    phone_number = models.CharField(unique=True, max_length=10)
    photo = models.TextField()
    birth_date = models.DateField()
    position = models.ForeignKey(EmployeePosition, on_delete=models.CASCADE)
    end_date = models.DateField(blank=True, null=True)
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = 'Employee Profiles'

    def get_full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def __str__(self):
        return '{}'.format(self.get_full_name())

    def save(self, *args, **kwargs):
        self.is_active = True
        self.is_superuser = False
        if not is_password_usable(self.password):
            self.password = make_password(self.password)
        super(EmployeeProfile, self).save(*args, **kwargs)


class EmployeeSalary(models.Model):
    STATUS = (
        ('Pending', 'Pending'),
        ('Paid', 'Paid')
    )
    employee = models.ForeignKey(EmployeeProfile, on_delete=models.CASCADE)
    status = models.CharField(max_length=20, choices=STATUS)
    date_created = models.DateField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = 'Employee Salaries'

    def __str__(self):
        return '{}'.format(self.employee)
