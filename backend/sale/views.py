from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView


class SalesListView(LoginRequiredMixin, TemplateView):
    template_name = "sales/sales_list.html"

    def get_context_data(self, **kwargs):
        context = super(SalesListView, self).get_context_data()
        context['title'] = "Sales"
        context['sales_list'] = "active"

        return context
