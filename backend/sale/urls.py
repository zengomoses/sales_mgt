from django.urls import path

from sale.views import SalesListView

urlpatterns = [
    path('list', SalesListView.as_view(), name='sales_list'),
]