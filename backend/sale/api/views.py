from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView

from sale.api.serializers import SalesTransactionCreateAPISerializer, SalesTransactionListAPISerializer, \
    SalesTransactionRetrieveAPISerializer, SalesItemRetrieveAPISerializer, SalesItemListAPISerializer, \
    SalesItemCreateAPISerializer
from sale.models import SalesTransaction, SalesItem


class SalesTransactionCreateAPIView(LoginRequiredMixin, CreateAPIView):
    queryset = SalesTransaction.objects.all()
    serializer_class = SalesTransactionCreateAPISerializer


class SalesTransactionListAPIView(LoginRequiredMixin, ListAPIView):
    queryset = SalesTransaction.objects.all()
    serializer_class = SalesTransactionListAPISerializer


class SalesTransactionRetrieveAPIView(LoginRequiredMixin, RetrieveAPIView):
    queryset = SalesTransaction.objects.all()
    serializer_class = SalesTransactionRetrieveAPISerializer


class SalesItemCreateAPIView(LoginRequiredMixin, CreateAPIView):
    queryset = SalesItem.objects.all()
    serializer_class = SalesItemCreateAPISerializer


class SalesItemListAPIView(LoginRequiredMixin, ListAPIView):
    queryset = SalesItem.objects.all()
    serializer_class = SalesItemListAPISerializer


class SalesItemRetrieveAPIView(LoginRequiredMixin, ListAPIView):
    serializer_class = SalesItemRetrieveAPISerializer

    def get_queryset(self):
        """
        This view should return a list of all products in in transaction.
        """
        transaction_id = self.kwargs['transaction_id']
        queryset = SalesItem.objects.filter(transaction_id=transaction_id)

        return queryset

