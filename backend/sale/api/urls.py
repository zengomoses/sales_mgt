from django.urls import path

from sale.api.views import SalesTransactionCreateAPIView, SalesTransactionListAPIView, SalesTransactionRetrieveAPIView, \
    SalesItemCreateAPIView, SalesItemListAPIView, SalesItemRetrieveAPIView

urlpatterns = [
    path('transaction/create', SalesTransactionCreateAPIView.as_view()),
    path('transaction/list', SalesTransactionListAPIView.as_view()),
    path('transaction/retrieve/<int:pk>', SalesTransactionRetrieveAPIView.as_view()),
    path('item/create', SalesItemCreateAPIView.as_view()),
    path('item/list/', SalesItemListAPIView.as_view()),
    path('item/retrieve/<int:transaction_id>', SalesItemRetrieveAPIView.as_view())
]