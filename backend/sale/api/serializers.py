from rest_framework.relations import StringRelatedField
from rest_framework.serializers import ModelSerializer

from sale.models import SalesTransaction, SalesItem


class SalesTransactionCreateAPISerializer(ModelSerializer):
    class Meta:
        model = SalesTransaction
        fields = "__all__"


class SalesTransactionListAPISerializer(ModelSerializer):
    added_by = StringRelatedField()

    class Meta:
        model = SalesTransaction
        fields = "__all__"


class SalesTransactionRetrieveAPISerializer(ModelSerializer):
    added_by = StringRelatedField()

    class Meta:
        model = SalesTransaction
        fields = "__all__"


class SalesItemCreateAPISerializer(ModelSerializer):
    class Meta:
        model = SalesItem
        fields = "__all__"


class SalesItemListAPISerializer(ModelSerializer):
    class Meta:
        model = SalesItem
        fields = "__all__"


class SalesItemRetrieveAPISerializer(ModelSerializer):
    product_id = StringRelatedField()

    class Meta:
        model = SalesItem
        fields = "__all__"
