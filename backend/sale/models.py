from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.db.models import F
from django.utils.timezone import now

from products.models import Product


class SalesTransaction(models.Model):
    transaction = models.CharField(max_length=25, unique=True)
    amount = models.FloatField()
    discount = models.FloatField()
    date_created = models.DateTimeField(default=now())
    added_by = models.ForeignKey(User, related_name="user_transaction", on_delete=models.CASCADE)

    def __str__(self):
        return self.transaction


class SalesItem(models.Model):
    transaction_id = models.ForeignKey(SalesTransaction, related_name="sales_item_transaction", on_delete=models.CASCADE)
    product_id = models.ForeignKey(Product, related_name="sales_item_transaction", on_delete=models.CASCADE)
    quantity = models.IntegerField(default=0)
    date_created = models.DateField(default=now().date())

    def save(self, *args, **kwargs):
        if self._state.adding:
            Product.objects.filter(pk=self.product_id_id).update(incoming_inventory=F('incoming_inventory') - self.quantity, outgoing_inventory=F('outgoing_inventory') + self.quantity)

        super(SalesItem, self).save(*args, **kwargs)

    def __str__(self):
        return '{} {}'.format(self.product_id.brand, self.product_id.name)
