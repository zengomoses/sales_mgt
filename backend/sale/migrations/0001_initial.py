# Generated by Django 2.0.5 on 2018-06-06 22:54

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('products', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='SalesItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.IntegerField(default=0)),
                ('date_created', models.DateField(default=datetime.date(2018, 6, 6))),
                ('product_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sales_item_transaction', to='products.Product')),
            ],
        ),
        migrations.CreateModel(
            name='SalesTransaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('transaction', models.CharField(max_length=25, unique=True)),
                ('amount', models.FloatField()),
                ('discount', models.FloatField()),
                ('date_created', models.DateTimeField(default=datetime.datetime(2018, 6, 6, 22, 54, 15, 144724, tzinfo=utc))),
                ('added_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_transaction', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='salesitem',
            name='transaction_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sales_item_transaction', to='sale.SalesTransaction'),
        ),
    ]
