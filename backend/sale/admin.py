from django.contrib import admin

from sale.models import SalesItem, SalesTransaction
# Register your models here.
class SalesItemAdmin(admin.ModelAdmin):
    list_display = ('product_id','transaction_id','date_created')

admin.site.register(SalesItem, SalesItemAdmin)

class SalesTransactionAdmin(admin.ModelAdmin):
    list_display = ('transaction','amount','discount','date_created','added_by')

admin.site.register(SalesTransaction, SalesTransactionAdmin)