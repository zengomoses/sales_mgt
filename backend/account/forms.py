from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import AuthenticationForm, UsernameField
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.utils.translation import ugettext as _


class UserLoginForm(AuthenticationForm):
    username = UsernameField(
        max_length=254,
        widget=forms.TextInput(attrs={'autofocus': True, 'class': 'form-control required', 'placeholder': 'Username'}),
    )
    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form-control required', 'placeholder': 'Password'}),
    )
    error_messages = {
        'invalid_login': _("Please enter a correct %(username)s and password. "),
        'inactive': _("This account is inactive."),
    }

    def __init__(self, *args, **kwargs):
        super(UserLoginForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = _("Username")

    def clean_username(self):
        username = self.cleaned_data['username']
        return username

    def confirm_login_allowed(self, user):
        """
        Do not let user with inactive person contact to login.
        """
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )

        # if not user.is_superuser:
        #     raise forms.ValidationError(_("You are not allowed to login."))
        # super(UserLoginForm, self).confirm_login_allowed(user)
