from django.urls import path, include
from django.views.generic import RedirectView

from account.views import LoginView, LogoutView

urlpatterns = [
    path('', RedirectView.as_view(url='login', permanent=False)),
    path('login', LoginView.as_view(), name='login'),
    path('logout', LogoutView.as_view(), name='logout'),

    path('admin/', include('dashboard.urls')),

    path('admin/employee/', include('employee.urls')),
    path('admin/employee/api/v1/', include('employee.api.urls')),

    path('admin/products/', include('products.urls')),
    path('admin/products/api/v1/', include('products.api.urls')),

    path('pos', include('pos.urls')),

    path('admin/stock/', include('stock.urls')),
    path('admin/stock/api/v1/', include('stock.api.urls')),

    path('admin/sales/', include('sale.urls')),
    path('admin/sales/api/v1/', include('sale.api.urls')),
]
