from django import forms
from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME, login, logout
from django.utils.http import is_safe_url
from django.views.generic import TemplateView, FormView

from account.forms import UserLoginForm


class LoginView(FormView):
    template_name = "login.html"
    form_class = UserLoginForm

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        context[REDIRECT_FIELD_NAME] = self.request.GET.get(REDIRECT_FIELD_NAME)
        context['title'] = "Login"

        return context

    def get_form_kwargs(self):
        kwargs = super(LoginView, self).get_form_kwargs()
        kwargs['request'] = self.request

        return kwargs

    def get_form(self, form_class=None):
        form = super(LoginView, self).get_form(form_class)
        form.fields[REDIRECT_FIELD_NAME] = forms.CharField(
            widget=forms.HiddenInput,
            required=False,
            initial=self.request.GET.get(REDIRECT_FIELD_NAME)
        )
        return form

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)

        return super(LoginView, self).form_valid(form)

    def get_success_url(self):
        url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if url and is_safe_url(url, self.request.get_host()):
            return url
        return settings.LOGIN_REDIRECT_URL


class LogoutView(FormView):
    template_name = "login.html"
    form_class = UserLoginForm

    def dispatch(self, request, *args, **kwargs):
        logout(request)

        return super(LogoutView, self).dispatch(request, *args, **kwargs)

