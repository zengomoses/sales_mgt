from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Sum
from django.views.generic import TemplateView

from sale.models import SalesTransaction, SalesItem
from stock.models import StockItem


class DashboardView(LoginRequiredMixin, TemplateView):
    template_name = "dashboard.html"

    def get_total_sales_of_the_day(self):
        sales_count = SalesTransaction.objects.all().count()
        return sales_count

    def get_total_products_sold(self):
        quantity_count = SalesItem.objects.aggregate(total=Sum('quantity'))
        return quantity_count

    def get_total_sales_revenue(self):
        earnings = SalesTransaction.objects.aggregate(amount=Sum('amount'))
        return earnings

    def get_total_profits(self):
        earnings = StockItem.objects.values('product')
        return earnings

    def get_top_5_sold_products(self):
        result = SalesItem.objects.values('product_id__brand__name', 'product_id__name', 'transaction_id__amount').order_by('product_id').annotate(total=Sum('quantity'))[:5]
        print(result)
        return result

    def get_total_discounts(self):
        discount = SalesTransaction.objects.aggregate(amount=Sum('discount'))
        return discount

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data()
        context['title'] = "Dashboard"
        context['dashboard'] = "active"
        context['sales_count'] = self.get_total_sales_of_the_day()
        context['product_count'] = self.get_total_products_sold()
        context['earnings'] = self.get_total_sales_revenue()
        context['discount'] = self.get_total_discounts()
        context['top_5_sold'] = self.get_top_5_sold_products()

        return context
