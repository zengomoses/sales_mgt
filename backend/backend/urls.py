"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.generic import RedirectView
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='KASANDA-SIMS')
admin.site.site_title = 'KASANDA-SIMS'
admin.site.site_header = 'KASANDA-SALES & INVENTORY MANAGEMENT SYSTEM'
admin.site.name = 'KASANDA-SIMS'
admin.site.index_title = 'KASANDA-SIMS DATABASE'

urlpatterns = [
    path('django-admin', admin.site.urls),

    path('swagger/', schema_view),
    path('api/token/auth/', obtain_jwt_token),

    path('', RedirectView.as_view(url='account/', permanent=False)),
    path('account/', include('account.urls')),
    path('pos/', include('pos.urls')),
    path('pos/api/v1/', include('pos.api.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


