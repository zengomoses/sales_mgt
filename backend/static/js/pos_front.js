//Clearing payment form fields
$('#modal-btn-cancel').click(function () {
    $('#amount-paid').val("");
    $('#balance').text(0);
});
// Search Bar Jquery
$("#search-criteria").on("keyup", function () {
    var g = $(this).val();
    $(".product-name").each(function () {
        var s = $(this).text();
        if (s.indexOf(g) !== -1) {
            $(this).parent().parent().show();
        }
        else {
            $(this).parent().parent().hide();
        }
    });
});
//Disabling Payment Button on Hover if the sales table is empty
$('#btn-payment').hover(function () {
    var rowCount = $('#table-contents tr').length;
    if (rowCount === 0) {
        $(this).prop('disabled', true);
        $(this).attr('title', 'Sorry you cant pay, Please choose at least one product to proceed');
    }
}, function () {
    $(this).prop('disabled', false);
});
//Disable save button when Amount Paid Field is empty
$('#btn-payment-save').hover(function () {
    if ($('#amount-paid').val() === "") {
        $(this).prop('disabled', true);
        $(this).attr('title', 'Sorry you cant Save, Please Fill the Amount Paid to proceed');
    }
}, function () {
    $(this).prop('disabled', false);
});


function calculate_total_price() {
    $('.discount').val("");
    $('#table-sales').find('tbody tr').each(function (index) {
        var $tblRow = $(this);
        var qty = parseInt($tblRow.find("[name=qty]").val(), 10);
        var price = parseFloat($tblRow.find("[name=price]").val());
        var total = (price * qty);
        if (!isNaN(total)) {
            var grandTotal = 0;
            var discount = 0;
            $tblRow.find('td:last').text(total);
            $(".total").each(function () {
                var stval = parseFloat($(this).text());
                grandTotal += isNaN(stval) ? 0 : stval;
            });

            //Assigning values to bottom table
            var bottomtblRow = $('#bottom-tbl tr');
            bottomtblRow.find('.grandTotal').text('Tsh ' + addCommas(grandTotal));
            bottomtblRow.find('.SubTotal').text('Tsh ' + addCommas(grandTotal));
            var items = $('#table-sales tbody tr').length;
            bottomtblRow.find('.items').text(items + ' Items');

            //Assigning values to a payment form
            $('#payout').text(addCommas(grandTotal));
            amountSelector = $('#amount-paid');
            var amountPaid = parseFloat(amountSelector.text());
            var balance = amountPaid - grandTotal;
            if (!isNaN(balance)) {
                $('#balance').text(addCommas(balance));
            } else {
                $('#balance').text(0);
            }
            //Change a value of Balance on keyup
            amountSelector.keyup(function () {
                var amountPaid = parseFloat(amountSelector.val());
                /*grandTotal = parseFloat($('.grandTotal').val());*/
                /*alert(grandTotal);*/
                balance = amountPaid - grandTotal;
                if (!isNaN(balance)) {
                    $('#balance').text(addCommas(balance));
                } else {
                    $('#balance').text(0);
                }
            });

            bottomtblRow.find('#discount-amount').text('Tsh 0');

            //calculating Discount
            $('.discount').keyup(function () {
                document.addEventListener("keydown", KeyCheck);  //or however you are calling your method
                function KeyCheck(event) {
                    var KeyID = event.keyCode;
                    if (KeyID === 8) {
                        grandTotal = $('.SubTotal').text().replace('Tsh', "").replace(/,/g, '').trim();
                    }
                }

                discount = parseFloat($(this).val());
                if (!isNaN(discount)) {
                    grandTotal = $('.SubTotal').text().replace('Tsh', "").replace(/,/g, '').trim();
                    grandTotal = grandTotal - discount;
                    bottomtblRow.find('.grandTotal').text('Tsh ' + addCommas(grandTotal));
                    bottomtblRow.find('#discount-amount').text('Tsh ' + addCommas(discount));
                    $('#payout').text('Tsh ' + addCommas(grandTotal));
                } else {
                    bottomtblRow.find('.grandTotal').text('Tsh ' + addCommas(grandTotal));
                    bottomtblRow.find('#discount-amount').text('Tsh ' + addCommas(0));
                    $('#payout').text('Tsh ' + addCommas(grandTotal));
                }
            });
        }

        //Assigning a values on keyup of Quantity
        $tblRow.find("[name=qty]").keyup(function () {
            var qty = parseInt($tblRow.find("[name=qty]").val(), 10);
            var price = parseFloat($tblRow.find("[name=price]").val());
            var total = (price * qty);
            if (!isNaN(total)) {
                var grandTotal = 0;
                $tblRow.find('td:last').text(total);
                $(".total").each(function () {
                    var stval = parseFloat($(this).text());
                    grandTotal += isNaN(stval) ? 0 : stval;
                });
                bottomtblRow.find('.grandTotal').text('Tsh ' + addCommas(grandTotal));
                bottomtblRow.find('.SubTotal').text('Tsh ' + addCommas(grandTotal));
            }
            $('#payout').text(addCommas(grandTotal));
        });

    });
}

$(document).ready(function () {
    $.getJSON('api/v1/product/categories/', function (d) {
        data = d.results;
        $.each(data, function (key, category) {
            $('#categories-list').append('<li class="btn btn-sm grn-border catBtn" data-toggle="tab" href="#' + category.name.replace(" ", "_") + '">' + category.name + '</li>');
            $('#brand-content').append('<div role="tabpanel" class="tab-pane fade" id="' + category.name.replace(" ", "_") + '"></div>');
            brands = data[key].category_brands;
            $.each(brands, function (key, brand) {
                $('#' + brand.category.replace(" ", "_")).append('<button role="tabpanel" class="btn btn-xs btn-primary brand-btn" id="' + brand.id + '">' + brand.name + '</button>' + '&nbsp');
            });
        });
        $('.brand-btn').click(function () {
            $('#img-list').empty();

            var id = $(this).attr('id');
            $.getJSON('api/v1/product/' + id, function (products) {
                products = products.results;
                $.each(products, function (key, product) {
                    $('#img-list').append('<div class="thumbnail col-md-2">\n' +
                        '                    <span class="thumbnail product-image">\n' +
                        '                        <img src="' + product.picture + '" alt="image" class="image-size">\n' +
                        '                    </span>\n' +
                        '\n' +
                        '                    <div class="image-caption align-center">\n' +
                        '                             <span class="product-name">Name: ' + product.name + '</span><br>\n' +
                        '                            <span>Price: ' + product.unit_price + '</span><br>\n' +
                        '                            <span>Status: ' + product.status + '</span>\n' +
                        '                            <button type="button" class="btn btn-info btn-xs cart-btn" id="' + product.id + '">Add to cart</button>\n' +
                        '                    </div>\n' +
                        '                </div>');

                });

                $('.cart-btn').click(function () {
                    var product_id = $(this).attr('id');
                    $.getJSON('api/v1/product/retrieve/' + product_id, function (product_detail) {
                        var num = $('#table-contents tr td:eq(0):contains(' + product_id + ')').length;
                        if (num > 0) {
                            swal("Sorry", "The Product you are trying to add already Exists", "error");
                        } else {
                            $('#table-contents').append('<tr><td style="display:none;">' + product_detail.id + '</td>' +
                                '<td>' + product_detail.name + '</td><td><input type="text" class="text-field" disabled="disabled" value="' + product_detail.unit_price + '" name="price"></td>' +
                                '<td><input type="text" value="1" class="quantity" name="qty"/></td><td class="total">' + product_detail.unit_price + '</td></tr>');
                        }
                        calculate_total_price();
                    });
                });
            });
        });
    });

    $("#btn-payment-save").on("click", function () {
        var transactionId = generateTransactionId();
        var transactions = {
            'amount': $.trim($('.grandTotal').text().replace('Tsh', "").replace(/,/g, '')),
            'discount': $('#discount-amount').text().replace('Tsh', "").replace(/,/g, ''),
            'transaction': transactionId,
            'added_by': $('#user-id').val()
        };
        $.ajax({
            url: '/account/admin/sales/api/v1/transaction/create', //EndPoint Yako /URL yako ya POST,
            type: 'POST', // Request Method yako GET au POST,
            data: transactions,//Your data here,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            },
            success: function (response) {
            },
            error: function (xhr, errorThrown, result) {
            }
        }).done(function (response) {
            var TableData = [];
            $("#table-sales tbody tr").each(function (i, tr) {
                TableData[i] = {
                    'product_id': $(tr).find("td:eq(0)").text(),
                    'transaction_id': response.id,
                    'quantity': $(tr).find("td:eq(3)").find("input").val()
                }
            });
            var len = TableData.length;

            $.each(TableData, function (i, sales) {
                $.ajax({
                    url: '/account/admin/sales/api/v1/item/create', //EndPoint Yako /URL yako ya POST,
                    type: 'POST', // Request Method yako GET au POST,
                    data: sales,//Your data here,
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                    },
                    success: function (response) {
                        if (i === (len - 1)) {
                            /*  $(document).ajaxSuccess(function () {
                                  $('#payment-form').modal('hide');
                                  $.notify({
                                      title: "Successful : ",
                                      message: "Transaction successfully saved!",
                                      icon: 'fa fa-check'
                                  }, {
                                      type: "success",
                                      offset: 50
                                  });
                              });*/
                            $('#payment-form').modal('hide');
                            swal("Successful", "Transaction Successfully Saved!", "success");
                            setTimeout(function () {
                                location.reload();
                            }, 2000)
                        }
                    },
                    error: function (xhr, errorThrown, result) {
                        console.log(errorThrown);
                    }
                });
            });
        });
        $('#amount-paid').val("");
        $('.discount').val("");
    });
});

