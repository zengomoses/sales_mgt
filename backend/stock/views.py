from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView


class StockListView(LoginRequiredMixin, TemplateView):
    template_name = "stock/stock_list.html"

    def get_context_data(self, **kwargs):
        context = super(StockListView, self).get_context_data()
        context['title'] = "Stock"
        context['stock_list'] = "active"

        return context
