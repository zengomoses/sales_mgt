from rest_framework import serializers
from rest_framework.relations import StringRelatedField
from rest_framework.serializers import ModelSerializer

from stock.models import StockItem


class StockItemCreateSerializer(ModelSerializer):

    def __init__(self, *args, **kwargs):
        many = kwargs.pop('many', True)
        super(StockItemCreateSerializer, self).__init__(many=many, *args, **kwargs)

    class Meta:
        model = StockItem
        fields = ('product', 'buying_price', 'quantity',)


class StockItemListSerializer(ModelSerializer):
    product = StringRelatedField()
    quantity__sum = serializers.IntegerField()
    buying_price__sum = serializers.IntegerField()
    product__name = serializers.CharField()
    product__brand__name = serializers.CharField()

    class Meta:
        model = StockItem
        fields = ('product', 'product__name', 'product__brand__name', 'buying_price__sum', 'quantity__sum')
