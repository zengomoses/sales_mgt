from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Count, Sum
from rest_framework.generics import CreateAPIView, ListAPIView

from stock.api.serializers import StockItemCreateSerializer, StockItemListSerializer
from stock.models import StockItem


class StockItemCreateAPIView(LoginRequiredMixin, CreateAPIView):
    queryset = StockItem.objects.all()
    serializer_class = StockItemCreateSerializer


class StockItemListAPIView(LoginRequiredMixin, ListAPIView):
    queryset = StockItem.objects.all()
    serializer_class = StockItemListSerializer

    def get_queryset(self):
        queryset = StockItem.objects.values('product', 'product__name', 'product__brand__name').annotate(Sum('quantity'), Sum('buying_price')).order_by('product')
        print(queryset)
        return queryset