from django.urls import path

from stock.api.views import StockItemCreateAPIView, StockItemListAPIView

urlpatterns = [
    path('create', StockItemCreateAPIView.as_view()),
    path('list', StockItemListAPIView.as_view())
]