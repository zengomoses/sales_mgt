from django.urls import path

from stock.views import StockListView

urlpatterns = [
    path('list', StockListView.as_view(), name='stock_list')
]