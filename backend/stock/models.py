from django.db import models

# Create your models here.
from django.db.models import F
from django.utils.timezone import now

from products.models import Product
from products.utils import generate_stock_number


class StockItem(models.Model):
    stock_number = models.CharField(max_length=25, null=True, blank=True)
    product = models.ForeignKey(Product, related_name='stock_items', on_delete=models.CASCADE)
    buying_price = models.FloatField(default=0.0)
    quantity = models.IntegerField(default=0)
    date_added = models.DateTimeField(default=now())

    def __str__(self):
        return self.stock_number

    def save(self, *args, **kwargs):
        if self._state.adding:
            Product.objects.filter(pk=self.product.pk).update(incoming_inventory=F('incoming_inventory') + self.quantity)
            self.stock_number = generate_stock_number()
        super(StockItem, self).save(*args, **kwargs)

    def get_total_buying_price(self):
        total_price = self.buying_price * self.quantity
        return total_price
