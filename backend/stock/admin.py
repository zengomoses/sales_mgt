from django.contrib import admin

from stock.models import StockItem


class StockAdmin(admin.ModelAdmin):
    list_display = ('stock_number', 'product', 'buying_price', 'quantity', 'date_added')


admin.site.register(StockItem, StockAdmin)
