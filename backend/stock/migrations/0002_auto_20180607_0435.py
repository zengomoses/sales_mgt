# Generated by Django 2.0.5 on 2018-06-07 04:35

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('stock', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stockitem',
            name='date_added',
            field=models.DateTimeField(default=datetime.datetime(2018, 6, 7, 4, 35, 39, 392876, tzinfo=utc)),
        ),
    ]
